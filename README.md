# Calculateur d’attaque pour MyHordes

## À propos

Permet de calculer la probabilité de survivre à une attaque sur [MyHordes](https://myhordes.eu).

Basé sur le [code de MyHordes](https://gitlab.com/eternaltwin/myhordes/myhordes) ainsi que les parties publiques du [code de Hordes.fr](https://github.com/motion-twin/WebGamesArchives/tree/main/Hordes).

## Installation

- S’assurer que la version de Python 3 installée est bien 3.10 au minimum.
- Cloner ce dépôt (ou télécharger les fichiers).

## Utilisation
### Afficher une page d’aide
`py simulateur.py --help` (ou `-h`) permet d’afficher une page d’aide qui résume toutes les informations qui suivent.

### Création et édition de presets
Il est possible de créer un nouveau preset de ville de manière interactive en utilisant la commande `py simulateur.py --new-preset <name>` (ou `-np <name>`). Ce preset peut ensuite être édité de manière interactive avec la commande `--edit-preset <name>` (ou `-ep <name>`), ou dupliqué *puis* édité (sous un nouveau nom) avec la commande `--copy-preset <old_name> <new_name>` (ou `-cp <old_name> <new_name>`).

Les presets sont enregistrés dans le dossier `presets/` avec l’extension de fichier `.town`, mais ce sont de simples fichiers XML. Il est possible de les modifier manuellement avec n’importe quel outil adapté.

### Simulations
Pour charger un preset existant et l’utiliser dans le cadre d’une simulation, la commande à taper est `--load-preset <name>` (ou `-lp <name>`) ; si cet argument est omis, la ville sera créée de manière interactive pour la simulation **et ne sera pas sauvegardée**.

Il est possible de modifier temporairement (seulement pour une simulation) différentes valeurs de la ville :
- `--attack <value>` ou `-a <value>` pour définir l’attaque à une valeur exacte.
- `--attack <min>:<max>` ou `-a <min>:<max>` pour définir le minimum et le maximum de l’attaque. (Il est possible d’omettre une des deux valeurs pour conserver la valeur du preset : par exemple, `-a :500` conserve le minimum du preset, mais définit le maximum à 500 zombies.)
- `--defenses <value>` ou `-d <value>` pour définir la défense *de base* (hors temporaires / veille).
- `--defenses <base>:<temp>:<nightwatch>` ou `-d <b>:<t>:<n>` pour modifier les trois variables de défense (base, temporaire, veille). (Il est également possible d’omettre une ou deux des trois valeurs pour conserver la ou les valeurs du preset : par exemple, ::400 conserve les défenses et les défenses temporaires originales, mais définit les défenses de veille à 400.)

Il est possible de définir manuellement les arguments de la simulation ; si ceux-ci sont omis, ils devront être spécifiés de manière interactive avant le lancement de ladite simulation :
- `--samples <samples>` ou `-s <samples>` pour spécifier *a priori* le nombre d’itérations de l’attaque à exécuter, et éviter d’avoir à le préciser de manière interactive.
- `--version <version>` ou `-v <version>` pour spécifier la version de Hordes/MyHordes à utiliser pour les calculs (14 pour la S14 de Hordes, 15 ou 16 pour les saisons de MyHordes)

Certains arguments permettent d’afficher davantage de détails :
- `--verbose-details` ou `-vd` pour afficher des détails supplémentaires sur les villes (décomposition des défenses, statistiques des différents citoyens…)
- `--verbose-stats` ou `-vs` pour afficher des statistiques plus détaillées après une simulation (distribution détaillée, types de mort…)
- `--last-man-standing` ou `-lms` pour afficher les chances de chaque joueur d’obtenir la MU. Si plusieurs joueurs survivent à l’attaque nocturne, les chances d’avoir la MU ne seront pas calculées en tenant compte des prochains jours mais simplement (pour l’instant) équitablement réparties entre tous les survivants (par exemple si 2 personnes survivent à l’attaque, elles seront toutes deux affichées avec ~50 % de chances de MU, même si leurs défenses personnelles sont très différentes)

## Todo

- [x] - Implémenter l’algorithme de calcul de Hordes.fr en plus de celui de MyHordes.
- [x] - Permettre d’enregistrer des *presets* de villes, et de les enregistrer / charger en ligne de commande.
    - [x] - Permettre de modifier et de dupliquer les presets de ville (implémenter `--edit-preset` et `--copy-preset`).
- [x] - Permettre de lancer des simulations en une seule commande (avec une syntaxe du genre `simulateur.py -preset "cavernes_mediocres_j15.preset" -samples 10000`).
- [ ] - Prendre en compte qui veille, et les chances de mourir en veille.
- [x] - Prendre en compte les différents états.
- [ ] - Prendre en compte les goules.
- [ ] - Ne pas seulement afficher quels citoyens sont morts, mais aussi :
    - [ ] - lesquels sont terrorisés
	- [x] - combien de citoyens meurent / sont terrorisés au minimum, au maximum, et moyenne / médiane
