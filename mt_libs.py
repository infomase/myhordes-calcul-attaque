import math
import random

def random_spread(total, pools, max_pool_value = None):
	if total <= 0 or pools <= 0:
		return []
	if max_pool_value is not None and total / pools > max_pool_value:
		return [max_pool_value for _ in range(pools)]
	
	plist = [0 for _ in range(pools)]
	remain = total
	while remain > 0:
		move = math.ceil(total * random.randint(1, 8) * .01)
		if move > remain:
			move = remain
		pool = random.randrange(pools)
		if max_pool_value is not None and plist[pool] + move > max_pool_value:
			move = max_pool_value - plist[pool]
		plist[pool] += move
		remain -= move
	return plist