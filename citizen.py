import random
import xml.etree.ElementTree as ElementTree
from copy import copy, deepcopy

status_names = ["", "Désaltéré", "Soif", "Déshydraté", "Fatigue", "Rassasié", "Vaincre la mort", "Campeur avisé", "Clair", "Drogué", "Dépendant", "Immunisé", "Ivre", "Gueule de bois", "Terreur", "Blessure", "Convalescent", "Infection", "Goule", "Banni", "Campeur avisé"]
death_reasons = ["En ville", "Dehors", "Infection", "Manque", "Déshydratation"]
pre_attack_death_reasons = ["Dehors", "Infection", "Manque", "Déshydratation"]
attack_death_reasons = ["En ville"]
jobs = ["Habitant", "Éclaireur", "Fouineur", "Gardien", "Ermite", "Apprivoiseur", "Technicien", "Chaman"]

def unpack_statuses(statuses):
	result = []
	if statuses == "":
		return result
	for status in statuses.split("|"):
		result.append(status)
	return result

def pack_statuses(statuses):
	result = ""
	leadingSeparator = False
	for status in statuses:
		result = f"{result}|{status}" if leadingSeparator else status
		leadingSeparator = True
	return result

def from_element(element):
	if element is None or element.tag != "citizen":
		return None
	try:
		job = jobs[0]
		is_in_town = True
		is_camping = False
		survival_chances = 0
		is_watcher = False
		nightwatch_death_chance = 0
		nightwatch_terror_chance = 0
		death_reason = None
		death_day = -1
		home_level = 0
		attraction = 0.0
		wasAttacked = False
		for attribute in element.items():
			match attribute[0]:
				case "name":
					name = attribute[1]
				case "job":
					job = attribute[1]
				case "defenses":
					defenses = int(attribute[1])
				case "decoration":
					decoration = int(attribute[1])
				case "isAlive":
					is_alive = bool(int(attribute[1]))
				case "isInTown":
					is_in_town = bool(int(attribute[1]))
				case "isCamping":
					is_camping = bool(int(attribute[1]))
				case "survivalChances":
					survival_chances = int(attribute[1])
				case "isWatcher":
					is_watcher = bool(int(attribute[1]))
				case "nightwatchDeathChance":
					nightwatch_death_chance = int(attribute[1])
				case "nightwatchTerrorChance":
					nightwatch_terror_chance = int(attribute[1])
				case "deathReason":
					death_reason = attribute[1]
				case "deathDay":
					death_day = int(attribute[1])
				case "statuses":
					statuses = attribute[1]
				case "homeLevel":
					home_level = int(attribute[1])
				case "attraction":
					attraction = float(attribute[1])
				case "wasAttacked":
					was_attacked = bool(int(attribute[1]))
				case _:
					print(f"WARN : Unknown attribute « {attribute[0]} » encountered in citizen preset file.")
		
		return Citizen(name, job, defenses, decoration, statuses, is_alive, is_in_town, is_camping, home_level, survival_chances, is_watcher, nightwatch_death_chance, nightwatch_terror_chance, attraction, death_reason, death_day)
	except Exception as exception:
		print(f"An error occured while loading citizen attributes : {exception}.")
		return None

class Citizen:
	
	def __init__(self, name, job, defenses, decoration, statuses, is_alive = True, is_in_town = True, is_camping = False, home_level = 0, survival_chances = 0, is_watcher = False, nightwatch_death_chance = 0, nightwatch_terror_chance = 0, attraction = 0.0, was_attacked = False, death_reason = None, death_day = -1):
		
		self.name = name[:16] if type(name) is str else "???"
		self.job = job if job in jobs else jobs[0]
		self.defenses = defenses if defenses >= 0 else 0
		self.decoration = decoration if decoration >= 0 else 0
		self.statuses = statuses if type(statuses) is list else unpack_statuses(statuses) if type(statuses) == str else []
		self.is_alive = is_alive
		self.is_in_town = is_in_town
		self.is_camping = is_camping
		self.home_level = home_level
		self.survival_chances = min(100, max(0, survival_chances))
		self.is_watcher = is_watcher
		self.nightwatch_death_chance = min(100, max(0, nightwatch_death_chance))
		self.nightwatch_terror_chance = min(100, max(0, nightwatch_terror_chance))
		self.attraction = min(1.0, max(0.0, attraction))
		self.was_attacked = was_attacked
		self.death_reason = death_reason
		self.death_day = death_day
	
	def __deepcopy__(self, memo):
		return Citizen(self.name, self.job, self.defenses, self.decoration, deepcopy(self.statuses), self.is_alive, self.is_in_town, self.is_camping, self.home_level, self.survival_chances, self.is_watcher, self.nightwatch_death_chance, self.nightwatch_terror_chance, self.attraction, self.was_attacked, self.death_reason, self.death_day)
	
	def to_element(self):
		dict = {"name" : self.name,
				"job" : self.job,
				"isAlive" : str(int(self.is_alive)),
				"isInTown" : str(int(self.is_in_town)),
				"homeLevel" : str(self.home_level),
				"defenses" : str(self.defenses),
				"decoration" : str(self.decoration),
				"statuses" : self.pack_statuses()
				}
		if not self.is_alive:
			dict["deathReason"] = str(self.death_reason)
			dict["deathDay"] = str(self.death_day)
		if self.is_in_town:
			dict["isWatcher"] = str(int(self.is_watcher))
			dict["attraction"] = str(self.attraction)
			dict["wasAttacked"] = str(int(self.was_attacked))
			if self.is_watcher:
				dict["nightwatchDeathChance"] = str(self.nightwatch_death_chance)
				dict["nightwatchTerrorChance"] = str(self.nightwatch_terror_chance)
		else:
			dict["isCamping"] = str(int(self.is_camping))
			if self.is_camping:
				dict["survivalChances"] = str(self.survival_chances)
		return ElementTree.Element("citizen", dict)
	
	def to_xml(self, file):
		try:
			tree = ElementTree.ElementTree(self.to_element())
			ElementTree.indent(tree, space = "\t", level = 0)
			tree.write(file, encoding = "utf-8")
			return True
		except Exception as exception:
			print(f"Could not write citizen file « {file} » : {exception}.")
			return False
	
	# def attack_wrapper(self, zombies, day, apply_results = True):
		# if apply_results:
			# temp_citizen = self
		# else:
			# temp_citizen = Citizen(self.name, self.defenses, self.decoration, copy(self.statuses), self.is_alive)
		
		# if temp_citizen.pre_attack(day - 1):
			# if temp_citizen.attack(zombies, day):
				# temp_citizen.post_attack()
		# return temp_citizen
		
	
	def pre_attack(self, day):
		if "Déshydraté" in self.statuses and "Vaincre la mort" not in self.statuses:
			self.die("Déshydratation", day)
			return False
		if "Infection" in self.statuses and "Vaincre la mort" not in self.statuses:
			if random.random() < 0.5:
				self.die("Infection", day)
				return False
		if "Dépendant" in self.statuses and "Drogué" not in self.statuses and "Vaincre la mort" not in self.statuses:
			self.die("Manque", day)
			return False
		# TODO Ghoul hunger
		
		if not self.is_in_town:
			if not self.is_camping or self.survival_chances <= random.randrange(100):
				self.die("Dehors", day)
				return False
		return True
	
	def nightwatch(self, day):
		if random.randrange(100) < self.nightwatch_death_chance:
			self.die("En ville", day)
			return False
		if random.randrange(100) < self.nightwatch_terror_chance and "Terreur" not in self.statuses:
			self.statuses.append("Terreur")
		return True
	
	def attack(self, zombies, day):
		if self.defenses < zombies:
			self.die("En ville", day)
			return False
		if "Terreur" not in self.statuses:
			if random.random() < (1 - self.decoration / 100):
				self.statuses.append("Terreur")
		return True
	
	def attack_hfr(self, zombies, day):
		if self.defenses < zombies:
			self.die("En ville", day)
			return False
		if "Terreur" not in self.statuses:
			if random.random() < (1 - (0.1 if self.decoration > 25 else self.decoration / 100)
									- 0.05		# ranger sa maison, je pars du principe que tout le monde le fait…
									- 0.03):	# tenue de citoyen lavée, pareil…
				self.statuses.append("Terreur")
		return True
	
	def post_attack(self):
		if "Blessure" in self.statuses and "Infection" not in self.statuses:
			self.statuses.append("Infection")
		if "Gueule de bois" in self.statuses:
			self.statuses.remove("Gueule de bois")
		if "Ivre" in self.statuses:
			self.statuses.remove("Ivre")
			self.statuses.append("Gueule de bois")
		
		if "Désaltéré" not in self.statuses:
			if "Soif" in self.statuses:
				self.statuses.remove("Soif")
				self.statuses.append("Déshydraté")
			else:
				self.statuses.append("Soif")
		else:
			self.statuses.remove("Désaltéré")
		
		if "Campeur avisé" in self.statuses:
			self.statuses.remove("Campeur avisé")
		if self.is_camping:
			self.statuses.append("Campeur avisé")
		self.was_attacked = False
		return self
	
	def print_info(self):
		if not self.is_alive:
			return
		print(f"— {self.name:>16} : {self.defenses:>4} défenses, {self.decoration:>4} décoration." + ("" if self.is_in_town else " (dehors)"))
	
	def die(self, reason, day):
		self.is_alive = False
		self.death_reason = reason
		self.death_day = day
		self.defenses = 0
		self.statuses = []
	
	def pack_statuses(self):
		return pack_statuses(self.statuses)
	
	# def unpack_statuses(self, statuses):
		# self.statuses = unpack_statuses(statuses)