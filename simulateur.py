import sys
import math
import random
from pathlib import Path
from copy import copy, deepcopy
import os.path
import xml.etree.ElementTree as ElementTree
import town as Town
import citizen as Citizen

def print_usage():
	print("""\
Lorsqu’il est lancé sans aucun argument, ce simulateur est en mode « interactif ». Cela signifie que toutes les variables nécessaires sont définies par l’utilisateur au moment de l’exécution.
Il est possible d’enregistrer des presets afin d’accélérer ce processus.

Pour créer un preset, utiliser la commande :
	simulateur.py --new-preset <name> (ou -np <name>)
Pour modifier un preset, utiliser la commande :
	simulateur.py --edit-preset <name> (ou -ep <name>)
Pour dupliquer et modifier la copie d’un preset, utiliser la commande :
	simulateur.py --copy-preset <name> (ou -cp <name>)

Pour lancer une simulation à partir d’un preset, utiliser la commande :
	simulateur.py --load-preset <name> (ou -lp <name>)
Sans arguments supplémentaires, la partie « simulation » sera interactive. Il est possible de définir à l’avance les paramètres de la simulation, pour avoir un résultat directement :
	simulateur.py --load-preset <name> --samples <number> (ou -s <number>)

Pour afficher davantage de détails sur la ville, ajouter l’argument : --verbose-details (ou -vd).
Pour afficher davantage de statistiques après les simulations, ajouter l’argument : --verbose-stats (ou -vs).
	""")

def print_line(n = 44):
	print("—" * n)

def print_title():
	print("Simulateur de survie à l’attaque de MyHordes")

def print_s17_warning(target = "The interactive editor"):
	print(f"Warning : {target} has not yet been updated to reflect S17 changes and may not work as intended for S17 simulations.")

def main():
	force_continue = 0
	arguments = len(sys.argv)
	
	preset_to_load = ""
	preset_to_save = ""
	preset_function = ""
	override_min_attack = -1
	override_max_attack = -1
	override_defenses = -1
	override_defenses_temporary = -1
	override_defenses_nightwatch = -1
	override_max_home_level = -1
	override_guitar_played = -1
	override_day = -1
	override_in_town_citizens = -1
	samples = -1
	game_version = -1
	
	is_verbose_stats = False
	is_verbose_details = False
	do_lms_prevision = False
	
	PRESET_PATH = "./presets/"
	#USERDATA_FOLDER = "user/"
	#TEMPLATE_FOLDER = "sample/"
	TOWN_PRESET_EXTENSION = ".town"
	
	data_path = Path(PRESET_PATH)
	data_path.mkdir(exist_ok = True)
	
	for i in range(1, arguments):
		if force_continue > 0:
			force_continue -= 1
			continue
		match sys.argv[i]:
			case "-h" | "--help":
				if preset_function != "":
					print(sys.argv[i] + " is incompatible with " + preset_function)
					return -1
				preset_function = "--help"
				
			case "-o" | "--overflow":
				if preset_function != "":
					print(sys.argv[i] + " is incompatible with " + preset_function)
					return -1
				preset_function = "--overflow"
			
			case "-np" | "--new-preset":
				force_continue = 1
				if i >= arguments - 1:
					print("Expected preset name after " + sys.argv[i] + ".")
					return -1
				if preset_function != "":
					print(sys.argv[i] + " is incompatible with " + preset_function)
					return -1
				preset_function = "--new-preset"
				preset_to_save = sys.argv[i + 1]
				if not validate_preset_name(preset_to_save):
					print(preset_to_save + " is not a valid preset name.")
					return -1
				
			case "-ep" | "--edit-preset":
				force_continue = 1
				if i >= arguments - 1:
					print("Expected preset name after " + sys.argv[i] + ".")
					return -1
				if preset_function != "":
					print(sys.argv[i] + " is incompatible with " + preset_function)
					return -1
				preset_function = "--edit-preset"
				preset_to_load = sys.argv[i + 1]
				if not validate_preset_name(preset_to_load):
					print(preset_to_load + " is not a valid preset name.")
					return -1
			
			case "-cp" | "--copy-preset":
				force_continue = 2
				if i >= arguments - 2:
					print("Expected two preset names after " + sys.argv[i] + ".")
					return -1
				if sys.argv[i + 1] == sys.argv[i + 2]:
					print("Twe two preset names can’t be identical.")
					return -1
				if preset_function != "":
					print(sys.argv[i] + " is incompatible with " + preset_function)
					return -1
				preset_function = "--copy-preset"
				preset_to_load = sys.argv[i + 1]
				preset_to_save = sys.argv[i + 2]
				if not validate_preset_name(preset_to_load):
					print(preset_to_load + " is not a valid preset name.")
					return -1
				if not validate_preset_name(preset_to_save):
					print(preset_to_save + " is not a valid preset name.")
					return -1
				
			case "-lp" | "--load-preset":
				force_continue = 1
				if i >= arguments - 1:
					print("Expected preset name after " + sys.argv[i] + ".")
					return -1
				if preset_function != "":
					print(sys.argv[i] + " is incompatible with " + preset_function)
					return -1
				preset_function = "--load-preset"
				preset_to_load = sys.argv[i + 1]
				if not validate_preset_name(preset_to_load):
					print(preset_to_load + " is not a valid preset name.")
					return -1
				
			case "-a" | "--attack":
				force_continue = 1
				if i >= arguments - 1:
					print(f"Expected argument after {sys.argv[i]}.")
					return -1
				if preset_function != "--load-preset":
					print(f"A preset is required to use {sys.argv[i]}. Try --load-preset.")
					return -1
				if override_min_attack != -1 or override_max_attack != -1:
					print(f"Duplicate occurence of {sys.argv[i]}.")
					return -1
				override_attack = sys.argv[i + 1]
				if override_attack.isnumeric():
					override_min_attack = int(override_attack)
					override_max_attack = override_min_attack
					continue
				override_attack = override_attack.split(":")
				if len(override_attack) != 2:
					print(f"Wrong syntax for argument of {sys.argv[i]} : expected either <attack> or <minAttack>:<maxAttack>, got « {sys.argv[i + 1]} ».")
					return -1
				override_min_attack = override_attack[0]
				override_max_attack = override_attack[1]
				if override_min_attack == "":
					if override_max_attack == "":
						print(f"You should specify at least the minimum or the maximum attack for {sys.argv[i]}.")
						return -1
					override_min_attack = -1
				if override_max_attack == "":
					override_max_attack = -1
				try:
					override_min_attack = int(override_min_attack)
					override_max_attack = int(override_max_attack)
				except ValueError:
					print(f"Error while converting « {override_min_attack}:{override_max_attack} » to integers.")
					return -1
				if override_min_attack >= 0 and override_min_attack > override_max_attack:
					print(f"The maximum attack ({override_attack[1]}) can’t be less than the minimum attack ({override_attack[0]}).")
					return -1
			
			case "-d" | "--defenses":
				force_continue = 1
				if i >= arguments - 1:
					print(f"Expected argument after {sys.argv[i]}.")
					return -1
				if preset_function != "--load-preset":
					print(f"A preset is required to use {sys.argv[i]}. Try --load-preset.")
					return -1
				if override_defenses != -1 or override_defenses_nightwatch != -1 or override_defenses_temporary != -1:
					print(f"Duplicate occurence of {sys.argv[i]}.")
					return -1
				override_defenses_string = sys.argv[i + 1]
				if override_defenses_string.isnumeric():
					override_defenses = int(override_defenses_string)
					continue
				override_defenses_string = override_defenses_string.split(":")
				if len(override_defenses_string) != 3:
					print(f"Wrong syntax for argument of {sys.argv[i]} : expected either <defenses> or <defenses>:<tempDefenses>:<nightwatchDefenses>, got « {sys.argv[i + 1]} ».")
					return -1
				override_defenses = override_defenses_string[0]
				override_defenses_temporary = override_defenses_string[1]
				override_defenses_nightwatch = override_defenses_string[2]
				if override_defenses == "":
					if override_defenses_temporary == "" and override_defenses_nightwatch == "":
						print(f"You should specify at least the base, temporary or nightwatch defenses for {sys.argv[i]}.")
						return -1
					override_defenses = -1
				if override_defenses_temporary == "":
					override_defenses_temporary = -1
				if override_defenses_nightwatch == "":
					override_defenses_nightwatch = -1
				try:
					override_defenses = int(override_defenses)
					override_defenses_temporary = int(override_defenses_temporary)
					override_defenses_nightwatch = int(override_defenses_nightwatch)
				except ValueError:
					print(f"Error while converting « {override_defenses}:{override_defenses_temporary}:{override_defenses_nightwatch} » to integers.")
					return -1
			
			case "-s" | "--samples":
				force_continue = 1
				if i >= arguments - 1:
					print(f"Expected argument after {sys.argv[i]}.")
					return -1
				if preset_function != "--load-preset":
					print(f"A preset is required to use {sys.argv[i]}. Try --load-preset.")
					return -1
				if not sys.argv[i + 1].isnumeric() or int(sys.argv[i + 1]) < 1:
					print(f"Invalid argument for {sys.argv[i]} (expected positive integer, got {sys.argv[i + 1]}).")
					return -1
				if samples != -1:
					print(f"Duplicate occurence of {sys.argv[i]}.")
					return -1
				samples = int(sys.argv[i + 1])
			
			case "-mhl" | "--max-home-level":
				force_continue = 1
				if i >= arguments - 1:
					print(f"Expected argument after {sys.argv[i]}.")
					return -1
				if not preset_function in ("--load-preset", "--overflow"):
					print(f"A preset is required to use {sys.argv[i]}. Try --load-preset.")
					return -1
				if not sys.argv[i + 1].isnumeric() or int(sys.argv[i + 1]) < 0:
					print(f"Invalid argument for {sys.argv[i]} (expected positive integer, got {sys.argv[i + 1]}).")
					return -1
				if override_max_home_level != -1:
					print(f"Duplicate occurence of {sys.argv[i]}.")
					return -1
				override_max_home_level = int(sys.argv[i + 1])
			
			case "-gp" | "--guitar-played":
				force_continue = 1
				if i >= arguments - 1:
					print(f"Expected argument after {sys.argv[i]}.")
					return -1
				if not preset_function in ("--load-preset", "--overflow"):
					print(f"A preset is required to use {sys.argv[i]}. Try --load-preset.")
					return -1
				if not sys.argv[i + 1] in ("True", "true", "False", "false"):
					print(f"Invalid argument for {sys.argv[i]} (expected boolean, got {sys.argv[i + 1]}).")
					return -1
				if override_guitar_played != -1:
					print(f"Duplicate occurence of {sys.argv[i]}.")
					return -1
				override_guitar_played = 1 if sys.argv[i + 1] in ("True", "true") else 0
			
			case "-it" | "--in-town":
				force_continue = 1
				if i >= arguments - 1:
					print(f"Expected argument after {sys.argv[i]}.")
					return -1
				if preset_function != "--overflow":
					print(f"A preset is required to use {sys.argv[i]}. Try --overflow.")
					return -1
				if not sys.argv[i + 1].isnumeric() or int(sys.argv[i + 1]) < 0:
					print(f"Invalid argument for {sys.argv[i]} (expected positive integer, got {sys.argv[i + 1]}).")
					return -1
				if override_in_town_citizens != -1:
					print(f"Duplicate occurence of {sys.argv[i]}.")
					return -1
				override_in_town_citizens = int(sys.argv[i + 1])
			
			case "--day":
				force_continue = 1
				if i >= arguments - 1:
					print(f"Expected argument after {sys.argv[i]}.")
					return -1
				if preset_function != "--overflow":
					print(f"A preset is required to use {sys.argv[i]}. Try --overflow.")
					return -1
				if not sys.argv[i + 1].isnumeric() or int(sys.argv[i + 1]) < 0:
					print(f"Invalid argument for {sys.argv[i]} (expected positive integer, got {sys.argv[i + 1]}).")
					return -1
				if override_day != -1:
					print(f"Duplicate occurence of {sys.argv[i]}.")
					return -1
				override_day = int(sys.argv[i + 1])
			
			case "-vs" | "--verbose-stats":
				if preset_function == "":
					print("A preset is required to use " + sys.argv[i] + ".")
					return -1
				is_verbose_stats = True
			
			case "-vd" | "--verbose-details":
				if preset_function == "":
					print("A preset is required to use " + sys.argv[i] + ".")
					return -1
				is_verbose_details = True
			
			case "-lms" | "--last-man-standing":
				if preset_function == "":
					print("A preset is required to use " + sys.argv[i] + ".")
					return -1
				do_lms_prevision = True
			
			case "-l" | "--legacy":
				game_version = 14
			
			case "-v" | "--version":
				force_continue = 1
				if i >= arguments - 1:
					print(f"Expected argument after {sys.argv[i]}.")
					return -1
				if not preset_function in ("--load-preset", "--overflow"):
					print(f"A preset is required to use {sys.argv[i]}. Try --load-preset.")
					return -1
				if not sys.argv[i + 1].isnumeric() or int(sys.argv[i + 1]) < 1:
					print(f"Invalid argument for {sys.argv[i]} (expected positive integer, got {sys.argv[i + 1]}).")
					return -1
				if game_version != -1:
					print(f"Duplicate occurence of {sys.argv[i]}.")
					return -1
				game_version = int(sys.argv[i + 1])
				if game_version not in (14, 15, 16, 17):
					print("Only the following game versions are currently available : 14, 15, 16, 17.")
					return -1
			
			case _:
				print(f"Error : Unknown parameter {sys.argv[i]}.")
				return -1
				
	
	match preset_function:
		case "--help":
			return print_usage()
		case "--new-preset":
			return interactive_create_and_save_town(PRESET_PATH + preset_to_save + ".town")
		case "--edit-preset":
			return interactive_edit_and_save_town(PRESET_PATH + preset_to_load + ".town", PRESET_PATH + preset_to_load + ".town", allow_overwrite = True)
		case "--copy-preset":
			return interactive_edit_and_save_town(PRESET_PATH + preset_to_load + ".town", PRESET_PATH + preset_to_save + ".town")
		case "--load-preset":
			return preset_sim(PRESET_PATH + preset_to_load + ".town", samples, is_verbose_details, is_verbose_stats, season = game_version, do_lms_prevision = do_lms_prevision, override_defenses = (override_defenses, override_defenses_temporary, override_defenses_nightwatch), override_attack = (override_min_attack, override_max_attack), override_max_home_level = override_max_home_level, override_guitar_played = override_guitar_played)
		case "--overflow":
			return Town.print_max_active(season = game_version, day = override_day, in_town_citizens = override_in_town_citizens, max_home_level = override_max_home_level, guitar_played = override_guitar_played)
		case _:
			return interactive(samples, is_verbose_details, is_verbose_stats, season = game_version)

def validate_preset_name(name):
	if type(name) is not str:
		return False
	for char in name:
		if char not in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-æÆâÂêÊþÞÿŸûÛîÎœŒôÔäÄßëË‘`’'ðÐüÜïÏŀĿöÖùÙéÉèÈçÇàÀ":
			print(f"Invalid character « {char} » in preset name « {name} ».")
			return False
	return True

def interactive_create_and_save_town(filename):
	if os.path.isfile(filename):
		print(f"The file {filename} already exists, please use --edit-preset instead of --new-preset.")
		return -1
	town = interactive_create_town()
	print_line()
	if town.to_xml(filename):
		print(f"Le preset « {filename} » a été créé avec succès.")
		return 0
	return -1

def interactive_edit_and_save_town(file_to_load, file_to_save, allow_overwrite = False):
	if not os.path.isfile(file_to_load):
		print(f"The file {file_to_load} does not exist, try creating it with --new-preset.")
		return -1
	if not allow_overwrite and os.path.isfile(file_to_save):
		print(f"Can’t save the preset {file_to_save} with the same name as the original. Try using --edit-preset instead.")
		return -1
	town = interactive_edit_town(file_to_load)
	print_line()
	operation_str = "créé"
	if file_to_load == file_to_save:
		operation_str = "modifié"
		if input_text(f"Confirmer les changements apportés à {file_to_save} ? La version précédente sera perdue !", allowed_values = ["Oui", "Non"]) == "Non":
			print(f"Le preset {file_to_save} n’a pas été modifié.")
			return 1
	if town.to_xml(file_to_save):
		print(f"Le preset « {file_to_save} » a été {operation_str} avec succès.")
		return 0

def preset_sim(filename, samples = -1, verbose_details = False, verbose_stats = False, season = 17, do_lms_prevision = False, override_defenses = None, override_attack = None, override_max_home_level = -1, override_guitar_played = -1):
	if override_defenses is not None and override_defenses is not tuple:
		if len(override_defenses) != 3:
			return -1
	if (override_attack is not None and override_attack is not tuple):
		if len(override_attack) != 2:
			return -1
	
	print_title()
	print_line()
	town = Town.from_xml(filename)
	if town is None:
		return -1
	if override_defenses is not None:
		if override_defenses[0] >= 0:
			town.defenses = override_defenses[0]
		if override_defenses[1] >= 0:
			town.defenses_temporary = override_defenses[1]
		if override_defenses[2] >= 0:
			town.defenses_nightwatch = override_defenses[2]
	if override_attack is not None:
		if override_attack[0] >= 0:
			town.min_attack = override_attack[0]
		if override_attack[1] >= 0:
			town.max_attack = override_attack[1]
	if override_guitar_played >= 0:
		town.guitar_played = bool(override_guitar_played)
	if override_max_home_level >= 0:
		town.max_home_level = override_max_home_level
	
	town.print_info(season, verbose_details)
	interactive_simulate_attacks(town, samples, verbose_stats, season = season, do_lms_prevision = do_lms_prevision)
	return 0

def interactive(samples = -1, verbose_details = False, verbose_stats = False, season = 17):
	print_title()
	
	town = interactive_create_town()
	
	print_line()
	town.print_info(season, verbose_details)
	
	while True:
		interactive_simulate_attacks(town, samples, verbose_stats, season = season)
		if interactive_stop_exec():
			return 0
	
	return 0

def interactive_stop_exec():
	while True:
		print_line()
		answer = input("Faire une autre simulation ? (Oui / Non) ")
		if answer == "Oui":
			return False
		if answer == "Non":
			return True

def input_int(prompt, default_value = None, original_value = None, min = None, max = None):
	while True:
		if default_value is not None and type(default_value) is int:
			value = input(f"{prompt} (par défaut {default_value}) : ")
			if value == "":
				return default_value
		elif original_value is not None and type(original_value) is int:
			value = input(f"{prompt} (laisser vide pour conserver {original_value}) : ")
			if value == "":
				return original_value
		else:
			value = input(f"{prompt} : ")
		
		try:
			value = int(value)
		except ValueError:
			continue
		else:
			if min is not None and value < min:
				continue
			if max is not None and value > max:
				continue
			return value

def input_list(prompt, list, allow_duplicates = False, allowed_values = None, allowed_characters = None, custom_info = None):
	while True:
		value = input_text(prompt, allowed_values = allowed_values, allowed_characters = allowed_characters, custom_info = ("" if custom_info is None else custom_info) + "laisser vide pour terminer")
		if value == "":
			return
		if not allow_duplicates and value in list:
			print(f"L’entrée « {value} » se trouve déjà dans la liste.")
			continue
		list.append(value)

def input_text(prompt, allowed_values = None, default_value = None, original_value = None, allowed_characters = None, custom_info = None):
	bad_characters_found = False
	if custom_info is None:
		custom_info = ""
		sep = ""
		if type(allowed_values) is list:
			for av in allowed_values:
				custom_info = f"{custom_info}{sep}{av}"
				sep = " / "
	while True:
		if default_value is not None and type(default_value) is str:
			if custom_info != "":
				value = input(f"{prompt} ({custom_info} ; par défaut {default_value}) : ")
			else:
				value = input(f"{prompt} (par défaut {default_value}) : ")
			if value == "":
				return default_value
		elif original_value is not None and type(original_value) is str:
			if custom_info != "":
				value = input(f"{prompt} ({custom_info} ; laisser vide pour conserver {original_value}) : ")
			else:
				value = input(f"{prompt} (laisser vide pour conserver {original_value}) : ")
			if value == "":
				return original_value
		elif custom_info != "":
			value = input(f"{prompt} ({custom_info}) : ")
		else:
			value = input(f"{prompt} : ")
		
		if allowed_characters is not None and type(allowed_characters) is str:
			for char in value:
				if char not in allowed_characters:
					bad_characters_found = True
			if bad_characters_found:
				bad_characters_found = False
				continue
		if allowed_values is not None and type(allowed_values) is list:
			if value not in allowed_values:
				print(f"L’entrée « {value} » ne fait pas partie des valeurs autorisées.")
				continue
		return value

def interactive_create_town():
	print_s17_warning()
	print_line()
	print("Initialisation de la ville.")
	max_citizens = input_int("Nombre de citoyens", default_value = 40, min = 1)
	# alive_citizens = input_int("Nombre de citoyens vivants", min = 1, max = max_citizens)
	day = input_int("Jour", min = 1)
	defenses = input_int("Défenses (non temporaires)", min = 0)
	defenses_temporary = input_int("Défenses (temporaires)", default_value = 0, min = 0)
	defenses_nightwatch = input_int("Défenses (veille)", default_value = 0)
	min_attack = input_int("Attaque minimale", min = 0)
	max_attack = input_int("Attaque maximale", min = min_attack)
	
	new_town = Town.Town(max_citizens, day, defenses, defenses_temporary, defenses_nightwatch, min_attack, max_attack)
	
	interactive_edit_citizens(new_town)
	
	return new_town

def interactive_edit_town(filename):
	old_town = Town.from_xml(filename)
	if old_town is None:
		return -1
	print_s17_warning()
	
	max_citizens = input_int("Nombre de citoyens", original_value = old_town.max_citizens, min = 1)
	# alive_citizens = input_int("Nombre de citoyens vivants en ville", original_value = old_town.alive_citizens, min = 1, max = max_citizens)
	day = input_int("Jour", original_value = old_town.day, min = 1)
	defenses = input_int("Défenses (non temporaires)", original_value = old_town.defenses, min = 0)
	defenses_temporary = input_int("Défenses (temporaires)", original_value = old_town.defenses_temporary, min = 0)
	defenses_nightwatch = input_int("Défenses (veille)", original_value = old_town.defenses_nightwatch)
	min_attack = input_int("Attaque minimale", original_value = old_town.min_attack, min = 0)
	max_attack = input_int("Attaque maximale", original_value = old_town.max_attack, min = min_attack)
	
	new_town = Town.Town(max_citizens, day, defenses, defenses_temporary, defenses_nightwatch, min_attack, max_attack)
	new_citizens = deepcopy(old_town.citizens)
	new_town.add_citizens(new_citizens)
	
	interactive_edit_citizens(new_town)
	
	return new_town

def interactive_edit_citizens(town):
	while True:
		print_line()
		for i in range(town.num_citizens):
			citizen = town.citizens[i]
			print(f"[{i + 1}] : {(citizen.name if citizen.is_alive else '†' + citizen.name):>17s} ({citizen.defenses:>4} défenses, {citizen.decoration:>4} décoration)" + (f" — {'en ville' if citizen.is_in_town else 'en camping' if citizen.is_camping else 'dehors'}" if citizen.is_alive else ""))
		print(f"[{town.num_citizens + 1}] : Ajouter un autre citoyen…")
		citizen_to_edit = input_int("Éditer un citoyen (0 pour terminer, - pour supprimer)", min = -town.num_citizens, max = town.num_citizens + 1)
		
		if citizen_to_edit == 0:
			break
		if citizen_to_edit < 0:
			citizen_to_edit = -citizen_to_edit - 1
			if input_text(f"Confirmer la suppression du citoyen {town.citizens[citizen_to_edit].name} ?", allowed_values = ["Oui", "Non"]) == "Non":
				continue
			town.remove_citizen(citizen_to_edit)
			continue
		citizen_to_edit -= 1
		if citizen_to_edit == town.num_citizens:
			town.add_citizen(interactive_create_citizen(citizen_to_edit))
			continue
		modified_citizen = interactive_edit_citizen(citizen_to_edit, town.citizens[citizen_to_edit])
		if input_text(f"Confirmer les changements faits à {modified_citizen.name} ({modified_citizen.defenses} défenses, {modified_citizen.decoration} décoration) ?", allowed_values = ["Oui", "Non"]) == "Oui":
			town.citizens[citizen_to_edit] = modified_citizen

def interactive_edit_citizen(i, citizen_to_edit):
	print_line()
	print("Édition du citoyen n°" + str(i + 1))
	
	new_name = input_text("Changer le pseudo", original_value = citizen_to_edit.name, allowed_characters = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-æÆâÂêÊþÞÿŸûÛîÎœŒôÔäÄßëËéÉèÈàÀùÙ‘`’")
	new_job = input_text("Changer le métier", original_value = citizen_to_edit.job, allowed_values = Citizen.jobs, custom_info = "")
	new_is_alive = input_text("Toujours vivant ?" if citizen_to_edit.is_alive else "A-t-il ressucité ?", allowed_values = ["Oui", "Non"], original_value = "Oui" if citizen_to_edit.is_alive else "Non") == "Oui"
	
	if new_is_alive:
		new_defenses = input_int("Défenses personnelles", original_value = citizen_to_edit.defenses, min = 0)
		new_decoration = input_int("Décoration", original_value = citizen_to_edit.decoration, min = 0)
		new_statuses = copy(citizen_to_edit.statuses)
		for status in citizen_to_edit.statuses:
			if input_text(f"Conserver le statut « {status} » ?", allowed_values = ["Oui", "Non"]) == "Non":
				new_statuses.remove(status)
		if len(new_statuses) > 0:
			if len(new_statuses) == 1:
				print(f"Statut conservé : {new_statuses[0]}")
			else:
				print(f"Statuts conservés : {new_statuses}")
		input_list("Ajouter un état", new_statuses, allowed_values = Citizen.status_names)
		new_is_in_town = input_text("Toujours en ville ?" if citizen_to_edit.is_in_town else "De retour en ville ?", allowed_values = ["Oui", "Non"], original_value = "Oui" if citizen_to_edit.is_in_town else "Non") == "Oui"
		if new_is_in_town:
			new_is_camping = False
			new_survival_chances = 0
			new_is_watcher = input_text("Veille-t-il bien ce soir ?" if citizen_to_edit.is_watcher else "Va-t-il veiller ce soir ?", allowed_values = ["Oui", "Non"], original_value = "Oui" if citizen_to_edit.is_watcher else "Non") == "Oui"
			if new_is_watcher:
				new_nightwatch_death_chance = input_int("Modifier les chances de mourir", min = 0, max = 100, original_value = citizen_to_edit.nightwatch_death_chance)
				new_nightwatch_terror_chance = input_int("Modifier les chances de terreur", min = 0, max = 100, original_value = citizen_to_edit.nightwatch_terror_chance)
			else:
				new_nightwatch_death_chance = 0
				new_nightwatch_terror_chance = 0
		else:
			new_is_camping = input_text("Va-t-il bien camper ?" if citizen_to_edit.is_camping else "Va-t-il camper ?", allowed_values = ["Oui", "Non"], original_value = "Oui" if citizen_to_edit.is_camping else "Non") == "Oui"
			if new_is_camping:
				new_survival_chances = input_int("Modifier les chances de survie", min = 0, max = 100, original_value = citizen_to_edit.survival_chances)
			else:
				new_survival_chances = 0
			new_is_watcher = False
			new_nightwatch_death_chance = 0
			new_nightwatch_terror_chance = 0
		new_death_day = 0
		new_death_reason = 0
	else:
		new_defenses = 0
		new_decoration = 0
		new_statuses = []
		new_is_in_town = False
		new_is_camping = False
		new_survival_chances = 0
		new_is_watcher = False
		new_nightwatch_death_chance = 0
		new_nightwatch_terror_chance = 0
		new_death_day = input_int("Mort le jour", min = 0, original_value = citizen_to_edit.death_day)
		new_death_reason = input_text("Cause de la mort", allowed_values = Citizen.death_reasons, original_value = citizen_to_edit.death_reason)
		
	return Citizen.Citizen(new_name, new_job, new_defenses, new_decoration, new_statuses, new_is_alive, new_is_in_town, new_is_camping, new_survival_chances, new_is_watcher, new_nightwatch_death_chance, new_nightwatch_terror_chance, new_death_reason, new_death_day)

def interactive_create_citizen(i):
	print_line()
	print("Création du citoyen n°" + str(i + 1))
	
	citizen_name = input_text("Entrer le pseudo", allowed_characters = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-æÆâÂêÊþÞÿŸûÛîÎœŒôÔäÄßëËéÉèÈàÀùÙ‘`’")
	citizen_job = input_text("Entrer le métier", allowed_values = Citizen.jobs, default_value = Citizen.jobs[0], custom_info = "")
	citizen_is_alive = input_text("Citoyen en vie ?", allowed_values = ["Oui", "Non"], default_value = "Oui") == "Oui"
	if citizen_is_alive:
		citizen_defenses = input_int("Défenses personnelles", min = 0, default_value = 0)
		citizen_decoration = input_int("Décoration", min = 0, default_value = 0)
		citizen_statuses = []
		input_list("Ajouter un état", citizen_statuses, allowed_values = Citizen.status_names)
		citizen_is_in_town = input_text("Citoyen en ville ?", allowed_values = ["Oui", "Non"], default_value = "Oui") == "Oui"
		if citizen_is_in_town:
			citizen_is_camping = False
			citizen_survival_chances = 0
			citizen_is_watcher = input_text("Citoyen en veille ?", allowed_values = ["Oui", "Non"], default_value = "Non") == "Oui"
			if citizen_is_watcher:
				citizen_nightwatch_death_chance = input_int("Modifier les chances de mourir", min = 0, max = 100, default_value = 1 if citizen_job == "Gardien" else 5)
				citizen_nightwatch_terror_chance = input_int("Modifier les chances de terreur", min = 0, max = 100, default_value = 0)
			else:
				citizen_nightwatch_death_chance = 0
				citizen_nightwatch_terror_chance = 0
		else:
			citizen_is_camping = input_text("Va-t-il camper ?", allowed_values = ["Oui", "Non"], default_value = "Oui") == "Oui"
			if citizen_is_camping:
				citizen_survival_chances = input_int("Chances de survie", min = 0, max = 100, default_value = 100 if citizen_job == "Ermite" else 90)
			else:
				citizen_survival_chances = 0
			citizen_is_watcher = False
			citizen_nightwatch_death_chance = 0
			citizen_nightwatch_terror_chance = 0
		citizen_death_day = -1
		citizen_death_reason = None
	else:
		citizen_defenses = 0
		citizen_decoration = 0
		citizen_statuses = []
		citizen_is_in_town = False
		citizen_is_camping = False
		citizen_survival_chances = 0
		citizen_is_watcher = False
		citizen_nightwatch_death_chance = 0
		citizen_nightwatch_terror_chance = 0
		citizen_death_day = input_int("Mort le jour", min = 0, default_value = 0)
		citizen_death_reason = input_text("Cause de la mort", allowed_values = Citizen.death_reasons, default_value = "En ville")
	
	return Citizen.Citizen(citizen_name, citizen_job, citizen_defenses, citizen_decoration, citizen_statuses, citizen_is_alive, citizen_is_in_town, citizen_is_camping, citizen_survival_chances, citizen_is_watcher, citizen_nightwatch_death_chance, citizen_nightwatch_terror_chance, citizen_death_reason, citizen_death_day)
	
def interactive_simulate_attacks(town, samples = -1, verbose = False, season = 17, do_lms_prevision = False):
	print_line()
	print("Initialisation du simulateur en mode " + ("Hordes" if season <= 14 else "MyHordes") + f" S{season}.")
	
	if samples < 1:
		samples = input_int("Nombre d’essais", min = 1)
	
	death_count_sub_dictionary = {death_reason : 0 for death_reason in Citizen.death_reasons}
	death_count_sub_dictionary["Total"] = 0
	death_count = {citizen.name : copy(death_count_sub_dictionary) for citizen in town.list_alive_citizens()}
	lms_count = {citizen.name : 0 for citizen in town.list_in_town_citizens()}
	lms_total = 0
	sample_results = [0 for _ in range(town.alive_citizens + 1)]
	
	for _ in range(samples):
		temp_town = deepcopy(town)
		deaths = temp_town.attack(season)
		total_death_count = 0
		for death_type in deaths:
			total_death_count += len(deaths[death_type])
			for citizen_death in deaths[death_type]:
				death_count[citizen_death]["Total"] += 1
				death_count[citizen_death][death_type] += 1
		if temp_town.mort_ultime is not None:
			lms_count[temp_town.mort_ultime] += 1
			lms_total += 1
		elif do_lms_prevision:
			lms_count[random.choice(temp_town.list_in_town_citizens()).name] += 1
			lms_total += 1
		sample_results[total_death_count] += 1
	
	mean = 0
	median = -1
	min = -1
	max = -1
	count = 0
	for i in range(town.alive_citizens + 1):
		count += sample_results[i]
		mean += sample_results[i] * i
		if min == -1 and count >= 1:
			min = i
		if median == -1 and count > samples // 2:
			median = i
		if max == -1 and count >= samples:
			max = i
	mean /= count
	
	print_line()
	print(f"Le nombre de morts en ville est en moyenne de {mean}.")
	if verbose:
		print("Voici la distribution détaillée :")
		for i in range(max + 1):
			extra_text = ""
			if math.floor(mean) == i:
				if median == i:
					extra_text = f" -- Médiane/Moyenne ({mean})"
				else:
					extra_text = f" -- Moyenne ({mean})"
			elif median == i:
				extra_text = " -- Médiane"
			
			formatted_percentage = f"({(100 * sample_results[i] / samples):.2f}"
			print(f"{i:3d} → {str(sample_results[i]):>{int(math.log10(samples)) + 1}}/{samples} {formatted_percentage:>7} % du temps) {extra_text}")
	else:
		formatted_percentage_min = f"({(100 * sample_results[min] / samples):.2f}"
		formatted_percentage_max = f"({(100 * sample_results[max] / samples):.2f}"
		formatted_percentage_median = f"({(100 * sample_results[median] / samples):.2f}"
		print(f"Le minimum est de {min} {formatted_percentage_min:>7}% du temps) et le maximum de {max} {formatted_percentage_max:>7} % du temps).")
		print(f"La médiane est de {median} {formatted_percentage_median:>7} % du temps).")
	print_line()
	for citizen in sorted(death_count.items(), key = lambda x:x[1]["Total"], reverse = False):
		formatted_percentage = f"({(100 * citizen[1]['Total'] / samples):.2f}"
		if verbose:
			death_reasons = []
			for death_reason in citizen[1]:
				if citizen[1][death_reason] <= 0:
					continue
				if death_reason == "Total":
					continue
				death_reasons.append(death_reason)
			if len(death_reasons) == 1:
				print(f"{citizen[0]:>16s} est mort·e {str(citizen[1]['Total']):>{int(math.log10(samples)) + 1}} fois sur {samples} {formatted_percentage:>7} % du temps) : « {death_reasons[0]} ».")
			elif len(death_reasons) == 0:
				print(f"{citizen[0]:>16s} est mort·e {str(citizen[1]['Total']):>{int(math.log10(samples)) + 1}} fois sur {samples} {formatted_percentage:>7} % du temps).")
			else:
				print(f"{citizen[0]:>16s} est mort·e {str(citizen[1]['Total']):>{int(math.log10(samples)) + 1}} fois sur {samples} {formatted_percentage:>7} % du temps), dont :")
				for death_reason in death_reasons:
					formatted_percentage = f"({(100 * citizen[1][death_reason] / samples):.2f}"
					print(f"{' '*14}— {str(citizen[1][death_reason]):>{int(math.log10(samples)) + 1}} fois {formatted_percentage:>7} % du temps) « {death_reason} ».")
		else:
			print(f"{citizen[0]:>16s} est mort·e {str(citizen[1]['Total']):>{int(math.log10(samples)) + 1}} fois sur {samples} {formatted_percentage:>7} % du temps).")
	if do_lms_prevision or verbose and lms_total > 0:
		print_line()
		print("Voici les chances d’obtenir la Mort Ultime :")
		for citizen in sorted(lms_count.items(), key = lambda x:x[1], reverse = False):
			if citizen[1] <= 0:
				break
			formatted_percentage = f"({(100 * citizen[1] / samples):.2f}"
			print(f"{citizen[0]:>16s} : {str(citizen[1]):>{int(math.log10(samples)) + 1}} fois sur {samples} {formatted_percentage:>7} % du temps).")
	print_line()

if __name__ == '__main__':
	sys.exit(main())