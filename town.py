import math
import random
import mt_libs
import xml.etree.ElementTree as ElementTree
from copy import deepcopy
import citizen as Citizen

def from_xml(filename):
	try:
		tree = ElementTree.ElementTree(file = filename)
		if tree._root.tag != "town":
			print(f"The root element of the town preset {filename} is invalid.")
			return None
		return from_element(tree._root)
	except:
		print(f"An error occured while loading town preset « {filename} »")
		return None

def from_element(element):
	if element is None or element.tag != "town":
		return None
	
	mort_ultime = None
	max_home_level = 0
	guitar_played = False
	try:
		for attribute in element.items():
			match attribute[0]:
				case "maxCitizens":
					max_citizens = int(attribute[1])
				case "day":
					day = int(attribute[1])
				case "defenses":
					defenses = int(attribute[1])
				case "defensesTemporary":
					defenses_temporary = int(attribute[1])
				case "defensesNightwatch":
					defenses_nightwatch = int(attribute[1])
				case "minAttack":
					min_attack = int(attribute[1])
				case "maxAttack":
					max_attack = int(attribute[1])
				case "guitarPlayed":
					guitar_played = bool(int(attribute[1]))
				case "maxHomeLevel":
					max_home_level = int(attribute[1])
				case "mortUltime":
					mort_ultime = attribute[1]
				case "numCitizens":
					print(f"WARN : Deprecated attribute « {attribute[0]} » encountered in town preset file.")
				case _:
					print(f"WARN : Unknown attribute « {attribute[0]} » encountered in town preset file.")
	
		citizen_parent = element.find("citizens")
		if citizen_parent is not None:
			citizens = []
			for citizen_element in citizen_parent.iter():
				citizen = Citizen.from_element(citizen_element)
				if citizen is None:
					continue
				if len(citizens) >= max_citizens:
					raise ValueError(f"Too many citizens were found in town preset file (maximum {max_citizens}).")
				citizens.append(citizen)
		
		result = Town(max_citizens, day, defenses, defenses_temporary, defenses_nightwatch, min_attack, max_attack, guitar_played, max_home_level, mort_ultime)
		result.add_citizens(citizens)
		return result
	except Exception as exception:
		print(f"An error occured while loading town attributes : {exception}.")
		return None

def print_max_active(season, day, in_town_citizens = 0, max_home_level = 0, guitar_played = 0):
	match season:
		case 17:
			print(f"[SAISON 17] Jour {day} ; {in_town_citizens} citoyens en ville ; niveau max de maison {max_home_level} ; guitare {'' if guitar_played else 'non '}jouée")
			print(f"Débordement maximal : {max_active_mh_s17(day, in_town_citizens, max_home_level, guitar_played, 0.0)}—{max_active_mh_s17(day, in_town_citizens, max_home_level, guitar_played, 1.0)}")
		case 16:
			print(f"[SAISON 16] Jour {day} ; {in_town_citizens} citoyens en ville")
			print(f"Débordement maximal : {max_active_mh_s16(day, in_town_citizens)}")
		case 15:
			print(f"[SAISON 15] Jour {day}")
			print(f"Débordement maximal : {max_active_mh_s15(day)}")
		case 14:
			print(f"[SAISON 14] Jour {day} ; {in_town_citizens} citoyens en ville")
			print(f"Débordement maximal : {max_active_hfr(day, in_town_citizens)}")
		case _:
			print(f"Season {season} is not supported.")

def get_max_active(season, day, in_town_citizens = 0, max_home_level = -1, guitar_played = -1):
	match season:
		case 17:
			return max_active_mh_s17(day, in_town_citizens, max_home_level, guitar_played, 1.0)
		case 16:
			return max_active_mh_s16(day, in_town_citizens)
		case 15:
			return max_active_mh_s15(day)
		case 14:
			return max_active_hfr(day, in_town_citizens)
		case _:
			return 0
			
def get_min_active(season, day, in_town_citizens = 0, max_home_level = -1, guitar_played = -1):
	match season:
		case 17:
			return max_active_mh_s17(day, in_town_citizens, max_home_level, guitar_played, 0.0)
		case 16:
			return max_active_mh_s16(day, in_town_citizens)
		case 15:
			return max_active_mh_s15(day)
		case 14:
			return max_active_hfr(day, in_town_citizens)
		case _:
			return 0

def max_active_mh_s17(day, in_town, max_home_level, guitar_played, forced_randomness = None):
	b_level = max_home_level + 1.0 + ((random.randint(0, 50) / 100.0) if forced_randomness is None else forced_randomness * 0.5)
	return round((max(in_town, 10) / 3.0) * day * b_level * (1.1 if guitar_played else 1.0))

def max_active_mh_s16(day, in_town):
	return round(day * max(1.0, day / 10.0)) * max(15, in_town)

def max_active_mh_s15(day):
	max_active = 0
	if day < 4:
		max_active = zombies * random.uniform(0.45, 0.7)
	elif day < 15:
		max_active = day * 15
	elif day < 19:
		max_active = (day + 4) * 15
	elif day < 24:
		max_active = (day + 5) * 15
	else:
		max_active = (day + 6) * 15
	return max_active

def max_active_hfr(day, in_town):
	houses = max(in_town, 15)
	coef = max(1, day / 10)
	avg_damage_by_house = int(day * coef)
	return int(avg_damage_by_house * houses)

class Town:
	
	def __init__(self, num_citizens, day, defenses, defenses_temporary, defenses_nightwatch, min_attack, max_attack, guitar_played = False, max_home_level = 0, mort_ultime = None):
		
		self.citizens = []
		self.num_citizens = 0
		self.alive_citizens = 0
		self.in_town_citizens = 0
		self.max_citizens = num_citizens
		self.day = day if day > 0 else 1
		self.defenses = defenses
		self.defenses_temporary = defenses_temporary if defenses_temporary >= 0 else 0
		self.defenses_nightwatch = defenses_nightwatch
		self.min_attack = min_attack
		self.max_attack = max_attack
		self.guitar_played = guitar_played
		self.max_home_level = max_home_level
		self.mort_ultime = mort_ultime
	
	def __deepcopy__(self, memo):
		copy = Town(self.num_citizens, self.day, self.defenses, self.defenses_temporary, self.defenses_nightwatch, self.min_attack, self.max_attack, self.guitar_played, self.max_home_level, self.mort_ultime)
		copy.add_citizens(deepcopy(self.citizens))
		return copy

	def to_element(self):
		dict = {"maxCitizens" : str(self.max_citizens),
				"day" : str(self.day),
				"defenses" : str(self.defenses),
				"defensesTemporary" : str(self.defenses_temporary),
				"defensesNightwatch" : str(self.defenses_nightwatch),
				"minAttack" : str(self.min_attack),
				"maxAttack" : str(self.max_attack)
				}
		if self.guitar_played is not None:
			dict["guitarPlayed"] = str(int(self.guitar_played))
		if self.max_home_level is not None:
			dict["maxHomeLevel"] = str(self.max_home_level)
		if self.mort_ultime is not None:
			dict["mortUltime"] = str(self.mort_ultime)
		root = ElementTree.Element("town", dict)
		
		citizen_parent = ElementTree.Element("citizens")
		for citizen in self.citizens:
			current_citizen = citizen.to_element()
			if current_citizen is None:
				continue
			citizen_parent.append(current_citizen)
		root.append(citizen_parent)
		return root
	
	def to_xml(self, file):
		try:
			tree = ElementTree.ElementTree(self.to_element())
			ElementTree.indent(tree, space = "\t", level = 0)
			tree.write(file, encoding = "utf-8")
			return True
		except Exception as exception:
			print(f"Could not write town file « {file} » : {exception}.")
			return False
	
	def print_info(self, season, verbose):
		self.recalculate_citizens()
		total_defenses = (self.defenses + self.defenses_temporary + self.defenses_nightwatch)
		print("Jour " + str(self.day) + ", " + str(self.alive_citizens) + " citoyens en vie (sur " + str(self.num_citizens) + (") : " if verbose else ")."))
		if verbose:
			for citizen in self.citizens:
				citizen.print_info()
			print("")
		print(f"{'Attaque prévue':>20s} : {f'{self.min_attack:6}' if self.min_attack == self.max_attack else f'{self.min_attack}–{self.max_attack}':>13} zombies")
		print(f"{'Défenses':>20s} : {total_defenses:{6 if self.min_attack == self.max_attack else 13}}")
		if verbose:
			if self.defenses_temporary > 0:
				print(f"{'(dont temporaires':>20s} : {self.defenses_temporary:{6 if self.min_attack == self.max_attack else 13}})")
			if self.defenses_nightwatch > 0:
				print(f"{'(dont veille':>20s} : {self.defenses_nightwatch:{6 if self.min_attack == self.max_attack else 13}})")
		print(f"{'Débordement prévu':>20s} : {f'{max(0, self.min_attack - total_defenses):6}' if self.min_attack == self.max_attack else f'{max(0, self.min_attack - total_defenses)}–{max(0, self.max_attack - total_defenses)}':>13s} zombies")
		
		max_active = get_max_active(season, self.day, self.in_town_citizens, self.max_home_level, self.guitar_played)
		min_active = get_min_active(season, self.day, self.in_town_citizens, self.max_home_level, self.guitar_played)
		print(f"{'Débordement plafonné':>20s} : {f'{max_active:6}' if max_active == min_active else f'{min_active}–{max_active}':>13s} zombies")
	
	def list_alive_citizens(self):
		result = []
		for citizen in self.citizens:
			if citizen.is_alive:
				result.append(citizen)
		return result
	
	def list_in_town_citizens(self):
		result = []
		for citizen in self.citizens:
			if citizen.is_alive and citizen.is_in_town:
				result.append(citizen)
		return result
	
	def add_citizen(self, citizen, count = 1):
		if self.num_citizens + count > self.max_citizens:
			print(f"WARN : Cannot add more than {self.max_citizens} citizens.")
			return False
		for _ in range(count):
			self.citizens.append(citizen)
			self.num_citizens += 1
			if citizen.is_alive:
				self.alive_citizens += 1
				if citizen.is_in_town:
					self.in_town_citizens += 1
		return True
	
	def add_citizens(self, citizens_to_add):
		if type(citizens_to_add) is not list or len(citizens_to_add) < 1:
			return False
		for citizen in citizens_to_add:
			self.add_citizen(citizen)
		return True
	
	def remove_citizen(self, citizen):
		if self.num_citizens <= citizen:
			print(f"WARN : Cannot remove nonexisting citizen n°{citizen}.")
			return False
		if self.citizens[citizen].is_alive:
			self.alive_citizens -= 1
			if self.citizens[citizen].is_in_town:
				self.in_town_citizens -= 1
		self.num_citizens -= 1
		self.citizens.pop(citizen)
		return True
	
	def fill_citizens(self, up_to = -1):
		if up_to == -1:
			up_to = self.max_citizens
		while self.num_citizens < self.max_citizens:
			self.num_citizens += 1
			self.citizens.append(citizen.Citizen(None, 0, 0, [], False))
	
	def attack(self, season):
		zombies = random.randint(self.min_attack, self.max_attack)
		
		deaths = self.pre_attack()
		match season:
			case 17:
				deaths |= self.attack_mh_s17(zombies)
			case 16:
				deaths |= self.attack_mh_s16(zombies)
			case 15:
				deaths |= self.attack_mh_s15(zombies)
			case _:
				deaths |= self.attack_hfr(zombies)
		self.post_attack()
		
		return deaths
	
	def pre_attack(self):
		deaths = {death_reason : [] for death_reason in Citizen.pre_attack_death_reasons}
		for i in range(self.num_citizens):
			if not self.citizens[i].is_alive:
				continue
			if not self.citizens[i].pre_attack(self.day - 1):
				deaths[self.citizens[i].death_reason].append(self.citizens[i].name)
		
		self.recalculate_citizens()
		return deaths
	
	def post_attack(self):
		for i in range(self.num_citizens):
			if not self.citizens[i].is_alive:
				continue
			self.citizens[i].post_attack()
		
		if self.in_town_citizens == 0 and self.mort_ultime == None:
			if self.day < 5:
				return
			citizen_eligible = []
			for citizen in self.citizens:
				if citizen.is_alive:
					continue
				if citizen.death_day < self.day:
					continue
				if citizen.death_reason != "En ville":
					continue
				citizen_eligible.append(citizen.name)
			if citizen_eligible:
				self.mort_ultime = random.choice(citizen_eligible)
	
	def attack_mh_s17(self, zombies):
		if self.in_town_citizens <= 0:
			return []
	
		zombies_overflow = zombies - self.defenses - self.defenses_temporary - self.defenses_nightwatch
		
		citizens_random = self.list_in_town_citizens()
	
		max_active = max_active_mh_s17(self.day, self.in_town_citizens, self.max_home_level, self.guitar_played)
		
		attacked_citizens = min(10 + 2 * math.floor(max(0, self.day - 10) / 2.0), math.ceil(self.in_town_citizens * 1.0))
		attacking_zombies = min(max_active, zombies_overflow)
		
		attract_targets = []
		detracted_zombies = 0
		for random_citizen in citizens_random:
			if random_citizen.attraction > 0:
				attract_targets.append(random_citizen)
				detracted_zombies += int(round(attacking_zombies * random_citizen.attraction))
		attacking_zombies -= detracted_zombies
		
		targets = random.sample(citizens_random, attacked_citizens)
		if len(targets) <= 0:
			return []
		random.shuffle(targets)
		
		repartition = [random.random() for _ in range(attacked_citizens)]
		
		repartition[random.randrange(attacked_citizens)] += 0.3
	
		repartition_sum = sum(repartition)
		attacking_zombies_cached = attacking_zombies
		for i in range(attacked_citizens):
			repartition[i] = max(0, min(attacking_zombies_cached, round(attacking_zombies * repartition[i] / repartition_sum)))
			attacking_zombies_cached -= repartition[i]
		
		while attacking_zombies_cached > 0:
			repartition[random.randrange(attacked_citizens)] += 1
			attacking_zombies_cached -= 1
		
		deaths = {death_reason : [] for death_reason in Citizen.attack_death_reasons}
		repartition.sort(reverse=True)
		
		for i in range(attacked_citizens):
			force = repartition[i] + round(targets[i].attraction * (attacking_zombies + detracted_zombies))
			targets[i].was_attacked = True
			if not targets[i].attack(force, self.day):
				deaths[targets[i].death_reason].append(targets[i].name)
		
		for attract_target in attract_targets:
			force = round(attract_target.attraction * (attacking_zombies + detracted_zombies))
			if not attract_target.was_attacked:
				attract_target.was_attacked = True
				if not attract_target.attack(force, self.day):
					deaths[attract_target.death_reason].append(attract_target.name)
		
		self.recalculate_citizens()
		return deaths
	
	def attack_mh_s16(self, zombies):
		if self.in_town_citizens <= 0:
			return []
	
		zombies_overflow = zombies - self.defenses - self.defenses_temporary - self.defenses_nightwatch
		
		citizens_random = self.list_in_town_citizens()
		random.shuffle(citizens_random)
	
		max_active = max_active_mh_s16(self.day, self.in_town_citizens)
		
		attacked_citizens = min(10, math.ceil(self.in_town_citizens * 1.0))
		attacking_zombies = min(max_active, zombies_overflow)
		targets = random.sample(citizens_random, attacked_citizens)
		if len(targets) <= 0:
			return []
		
		repartition = [random.random() for _ in range(attacked_citizens)]
		
		repartition[random.randrange(attacked_citizens)] += 0.3
	
		repartition_sum = sum(repartition)
		attacking_zombies_cached = attacking_zombies
		for i in range(attacked_citizens):
			repartition[i] = max(0, min(attacking_zombies_cached, round(attacking_zombies * repartition[i] / repartition_sum)))
			attacking_zombies_cached -= repartition[i]
		
		while attacking_zombies_cached > 0:
			repartition[random.randrange(attacked_citizens)] += 1
			attacking_zombies_cached -= 1
		
		deaths = {death_reason : [] for death_reason in Citizen.attack_death_reasons}
		repartition.sort(reverse=True)
		
		for i in range(attacked_citizens):
			force = repartition[i]
			if not targets[i].attack(force, self.day):
				deaths[targets[i].death_reason].append(targets[i].name)
		
		self.recalculate_citizens()
		return deaths
	
	def attack_mh_s15(self, zombies):
		if self.in_town_citizens <= 0:
			return []
	
		zombies_overflow = zombies - self.defenses - self.defenses_temporary - self.defenses_nightwatch
		
		citizens_random = self.list_in_town_citizens()
		random.shuffle(citizens_random)
	
		max_active = max_active_mh_s15(self.day)
		
		attacked_citizens = min(10, math.ceil(self.in_town_citizens * 0.85))
		attacking_zombies = min(max_active, zombies_overflow)
		targets = random.sample(citizens_random, attacked_citizens)
		if len(targets) <= 0:
			return []
		
		repartition = [random.random() for _ in range(attacked_citizens)]
		
		repartition[random.randrange(attacked_citizens)] += 0.3
	
		repartition_sum = sum(repartition)
		attacking_zombies_cached = attacking_zombies
		for i in range(attacked_citizens):
			repartition[i] = max(0, min(attacking_zombies_cached, round(attacking_zombies * repartition[i] / repartition_sum)))
			attacking_zombies_cached -= repartition[i]
		
		while attacking_zombies_cached > 0:
			repartition[random.randrange(attacked_citizens)] += 1
			attacking_zombies_cached -= 1
		
		deaths = {death_reason : [] for death_reason in Citizen.attack_death_reasons}
		repartition.sort(reverse=True)
		
		for i in range(attacked_citizens):
			force = repartition[i]
			if not targets[i].attack(force, self.day):
				deaths[targets[i].death_reason].append(targets[i].name)
		
		self.recalculate_citizens()
		return deaths
	
	def attack_hfr(self, zombies):
		if self.in_town_citizens <= 0:
			return []
		
		remaining_zombies = zombies - self.defenses - self.defenses_temporary - self.defenses_nightwatch
		
		attacked_players = self.list_in_town_citizens()
		random.shuffle(attacked_players)
		
		zombies_on_houses = min(remaining_zombies, max_active_hfr(self.day, self.in_town_citizens))
		repartition = mt_libs.random_spread(zombies_on_houses, self.in_town_citizens)
		repartition.sort()
		
		immune = math.floor(self.in_town_citizens * 0.15) if self.day < 6 else 0
		# discarded_zombies = remaining_zombies - zombies_on_houses
		# has_bamba
		
		deaths = {death_reason : [] for death_reason in Citizen.attack_death_reasons}
		
		for player in attacked_players:
			zombies_on_player = repartition.pop()
			if immune > 0:
				immune -= 1
				# discarded_zombies += zombies_on_player
				continue
			if not player.attack_hfr(zombies_on_player, self.day):
				deaths[player.death_reason].append(player.name)
		
		self.recalculate_citizens()
		return deaths
	
	def recalculate_citizens(self):
		self.alive_citizens = sum(x.is_alive for x in self.citizens)
		self.in_town_citizens = sum(x.is_alive and x.is_in_town for x in self.citizens)
		self.max_home_level = max(self.max_home_level, max(x.home_level if x.is_in_town else 0 for x in self.citizens))
