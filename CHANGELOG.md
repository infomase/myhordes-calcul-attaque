# Changelog

## 2025-02-24 (actuelle) — Attractivité
### Ajouts :
- Les citoyens enregistrent désormais 2 nouvelles variables, `attraction` pour leur valeur d’attractivité et `was_attacked` (inutilisé sauf pendant le calcul de l’attaque, pour enregistrer si un citoyen a été attaqué ou non).
### Modifications :
- Le calcul de l’attaque prend en compte l’attractivité des citoyens (calculée de la même manière que sur MH).

## 2024-10-13 (2557dffe) — Nouveau bugfix de la guitare
### Réparations :
- Le paramètre « guitar_played » est désormais bien converti en Int avant d’être converti en Bool pour éviter qu’il soit toujours défini à True s’il est non vide.

## 2024-10-12 (1381ca86) — Bugfix de la guitare
### Réparations :
- L’initialisation d’une ville définissait toujours la variable « guitar_played » à False. Elle récupère désormais correctement la valeur passée en paramètre.

## 2024-10-01 (61d6f47c) — Modification de la formule du débordement (commit ab21446e MyHordes)
### Modifications :
- La formule calculant le débordement plafonné utilise désormais une valeur plancher de 10 pour le nombre de citoyens en ville et en vie.

## 2024-10-01 (3a8103ba) — Bugfix de l’affichage du débordement plafonné dans print_info
### Réparations :
- L’appel de `print_info()` dans [town.py](town.py) appelle désormais `recalculate_citizens()` au préalable, pour afficher correctement la valeur du débordement plafonné dans le cas où `max_home_level` ne serait pas spécifié dans le fichier preset.

## 2024-10-01 (9c2ede08) — Meilleur affichage du débordement plafonné
### Ajouts :
- De nouveaux paramètres de ligne de commande pour [simulateur.py](simulateur.py) :
  - `--overflow` (`-o`) ne lance pas de simulation, mais affiche simplement la valeur du débordement maximal (valeur plafonnée) en fonction des différents arguments spécifiés
  - `--day` permet de définir le jour
  - `--in-town` (`-it`) permet de définir le nombre de citoyens en ville et en vie
- Plusieurs fonctions liées à l’affichage de la valeur du débordement ont été ajoutées à [town.py](town.py) : `print_max_active()` affiche la valeur minimale/maximale du débordement plafonné, tandis que `get_min_active()` et `get_max_active()` renvoie sous forme numérique les valeurs limite desdits débordements.
### Modifications :
- Les paramètres de [simulateur.py](simulateur.py) `--max-home-level`, `--guitar-played` et `--version` peuvent désormais être utilisés avec la fonction `--overflow` (en plus de `--load-preset` comme précédemment).
- Toutes les fonctions de calcul du débordement par saison dans [town.py](town.py) ont été sorties de la classe `Town` et demandent désormais qu’on leur fournisse des paramètres explicitement plutôt qu’une référence à une instance de ladite classe.
- La fonction `print_info()` de [town.py](town.py) demande désormais un paramètre `season`, et affiche également le débordement plafonné minimal/maximal (en plus du débordement non plafonné).
### Réparations :
- Un paramètre inconnu lors de l’appel à [simulateur.py](simulateur.py) affiche désormais un message d’erreur et interrompt l’exécution du programme, plutôt que d’être silencieusement ignoré.

## 2024-10-01 (24f00611) — Saison 17 de MyHordes (beta)
### Ajouts :
- Deux nouveaux paramètres sont enregistrés pour chaque ville ([town.py](town.py)) : `guitar_played` qui détermine si la guitare a été jouée (vaut `False` par défaut, et ne peut ̂être définie que par le fichier preset), et `max_home_level` qui détermine le niveau maximal de maison présent en ville (cette valeur peut être définie par l’utilisateur, et est automatiquement recalculée à chaque itération de la simulation par la fonction `recalculate_citizens()`).
- Deux nouveaux arguments lors de l’appel à [simulateur.py](simulateur.py) permettent de modifier les nouvelles valeurs du preset :
  - `-mhl <n>` ou `--max-home-level <n>` définit le niveau maximal de maison présent en ville
  - `-gp <b>` ou `--guitar-played <b>` définit si la guitare a été jouée ou non (attend un booléen, `True` ou `False`)
  Comme les autres overrides, ces modifications ne peuvent être appliquées qu’à une simulation lancée avec `--load-preset` (`-lp`).
- Dans [town.py](town.py), ajout des fonctions de calcul de l’attaque de la saison 17 (`attack_mh_s17()` et `max_active_mh_s17()`). Elles ne prennent pas encore en compte la mécanique d’attractivité, dont les détails ne sont pas encore connus.
- Les citoyens ([citizen.py](citizen.py)) peuvent désormais stocker une variable `home_level` qui détermine leur niveau de maison (0 correspondant à un lit de camp, jusqu’à 8 qui correspond à un château).
### Modifications :
- Le programme ([simulateur.py](simulateur.py)) supporte désormais la version **17** de MyHordes : il est donc possible de lancer une simulation avec l’argument `-v 17`.
- La saison par défaut d’une simulation, si aucun paramètre n’est précisé à l’aide de `--version` ou `-v`, est désormais de **17** au lieu de **16**.
- Dans [town.py](town.py), toutes les fonctions de simulation d’attaque des versions antérieures (`attack_mh_s16()`, `attack_mh_s15()` et `attack_hfr()`) ont désormais une fonction séparée qui détermine le maximum du débordement (respectivement `max_active_mh_s16()`, `max_active_mh_s15()` et `max_active_hfr()`).
- Une phrase d’avertissement (`print_s17_warning()` dans [simulateur.py](simulateur.py)) informe l’utilisateur que l’éditeur interactif n’a pas été mis à jour pour la saison 17 et qu’il ne supporte pas (encore) les nouveautés.

## 2024-05-22 (c0e8ac95) — Ajout licence
### Ajouts :
- Ajout du fichier `LICENSE` (MIT).

## 2024-01-14 (03ab66ac) — Saison 16 de MyHordes
### Ajouts :
- Le code du débordement et de l’attaque de la S16 de MyHordes est désormais disponible. Le paramètre `--legacy` (pour utiliser l’algorithme de Hordes.fr) est désormais équivalent à `--version 14` (ou `-v 14`) ; les autres options disponibles sont `-v 15` pour la S15 de MyHordes et `-v 16` pour la S16 de MyHordes.
### Modifications :
- Le readme explique désormais bien le fonctionnement de l’argument `-lms`.

## 2024-01-14 (32c5e140) — Calcul de la mort ultime
### Ajouts
- La mort ultime peut désormais être calculée par le programme (si l’option est activée avec l’argument `-lms` / `--last-man-standing`). L’estimation n’est pas exacte à plusieurs jours et devra être peaufinée mais elle donne déjà une idée de qui a le plus de chances de survivre à l’attaque de la nuit.

## 2023-09-27 (e3d8289e) — Algorithme de Hordes
### Ajouts
- Les citoyens qui veillent, et leurs pourcentages de chances d’être blessés / de mourir, sont désormais enregistrés dans les presets de ville. Cependant la fonctionnalité n’est pas encore complètement implémentée ; les statistiques finales ne sont pas affectées par ces valeurs.
- Le code de Hordes.fr a été ajouté (dans [simulateur.py](simulateur.py), `attack_hfr()`) ; il est possible d’utiliser ce code plutôt que celui de MyHordes dans une simulation, avec l’argument `--legacy`. Le fichier [mt_libs.py](mt_libs.py) reproduit des fonctions des bibliothèques de la Motion-Twin qui ne font pas à proprement parler partie du code source de Hordes.

## 2023-09-16 (612ddf20) — Nouvel alignement de l’attaque et des défenses
### Modifications
- Dans la fonction d’affichage des informations d’une ville ([town.py](town.py), `print_info()`), l’alignement de l’attaque et des défenses a été à nouveau modifié et est plus agréable à l’œil lorsque le minimum et le maximum sont différents.

## 2023-09-16 (edc37ce6) — Mort dans l’Outre-Monde
### Modifications
- Dans la fonction `pre_attack()` de [citizen.py](citizen.py), un citoyen peut désormais bien mourir dans l’Outre-Monde s’il est dehors, ou s’il échoue son lancer aléatoire pour son camping.
- Dans la fonction `post_attack()`, l’état `"Campeur avisé"` est désormais bien donné et retiré.

## 2023-09-16 (7f31e789) — Mise en page de l’attaque et des défenses
### Modifications
- Les informations sur l’attaque, les défenses et le débordement, dans la fonction d’affichage des informations d’une ville ([town.py](town.py), `print_info()`) sont désormais séparées par un saut de ligne de la liste des citoyens. L’attaque et les défenses sont désormais alignées.

## 2023-09-16 (90f50583) — Métiers et camping
### Ajouts
- Dans [citizen.py](citizen.py), la classe `Citizen` a désormais plusieurs variables supplémentaires : le métier, un booléen pour savoir s’il campe ou non, ainsi que ses chances de survie en camping.
- Une nouvelle variable de la classe `Town` de [town.py](town.py), `in_town_citizens`, compte le nombre de citoyens en vie.
### Modifications
- La liste des citoyens (affichée par des appels successifs à `Citizen.print_info()`) indique désormais si un citoyen est dehors.
- Le code servant à l’édition des citoyens d’une ville (dans [simulateur.py](simulateur.py), `interactive_edit_town()`) a été déplacé dans sa propre fonction `interactive_edit_citizens()`. Il est désormais partagé par les fonctions `interactive_edit_town()` et `interactive_create_town()`, ce qui permet dans la seconde moins de lourdeur dans l’ajout des citoyens (en plus de ne plus nécessiter de spécifier le nombre de citoyens *a priori*).
- Le code d’édition d’un citoyen individuel, `interactive_edit_citizen()`, permet désormais de renseigner toutes les variables d’un citoyen. Même chose pour le code de création, `interactive_create_citizen()`.
- La ligne qui affiche le nombre de zombies prévus (dans [town.py](town.py), `print_info()`) n’affiche plus désormais qu’un seul nombre si le minimum et le maximum sont identiques.
- Dans [town.py](town.py), le nombre de citoyens en vie (`alive_citizens`) et en ville (`in_town_citizens`) sont désormais recalculés avant et après chaque attaque.
### Réparations
- Dans [simulateur.py](simulateur.py), la fonction `input_text()` n’affiche plus d’informations si on définit `custom_info = ""` (jusqu’à présent, le comportement était identique à `None`).
- Le parser XML des presets de ville ([town.py](town.py), `from_element()`) vérifie désormais si la liste des citoyens est supérieure au maximum autorisé, et lance une exception si tel est le cas.
### Suppressions
- L’attribut de preset de ville `numCitizens` n’est plus accepté par le parser XML. ([town.py](town.py), `from_element()`).

## 2023-09-16 (1ec33394) — Corrections et mise en page
### Modifications
- Chaque itération d’une simulation crée désormais une copie de la ville plutôt que d’utiliser un argument `apply_results` pour éviter que les résultats ne soient pris en compte. En conséquence, la fonction de [citizen.py](citizen.py) `attack_wrapper()` n’existe plus ; cette fonctionnalité a été déplacée directement dans [town.py](town.py). De nombreuses autres fonctions ont été réécrites pour tenir compte de ces changements.
- L’affichage des informations des citoyens ([citizen.py](citizen.py)) et des simulations ([simulateur.py](simulateur.py)) est désormais mieux mis en page. Divers nombres et pourcentages sont désormais alignés les uns par rapport aux autres plutôt que d’être positionnés à l’arrache comme jusqu’à présent.
- Les motifs de mort (dans [citizen.py](citizen.py) ont été classés en deux catégories : `pre_attack_death_reasons` (tous sauf « En ville ») et `attack_death_reasons` (uniquement « En ville »).
### Réparations
- Un bug qui entraînait la non-prise en compte des états si on était immunisé à l’attaque en ville a été corrigé.
- Une majuscule a été ajoutée à `"Terreur"` dans [citizen.py](citizen.py) (dans la fonction `attack()`).
- Le nom des citoyen·ne·s est désormais limité à 16 caractères, comme sur Hordes.

## 2023-09-15 (cc13a7ae) — Déplacement des fonctions de conversion XML
### Modifications
- Les fonctions de [simulateur.py](simulateur.py) `citizen_to_xml()`, `citizen_to_element()` et `element_to_citizen()` ont été déplacées dans [citizen.py](citizen.py) : `element_to_citizen(element)` s’appelle désormais `from_element(element)`, et `citizen_to_xml(citizen, file)` et `citizen_to_element(citizen)` sont des méthodes de la classe `Citizen` et s’appellent respectivement `to_xml()` et `to_element()`.
- Les fonctions de [simulateur.py](simulateur.py) `town_to_xml()`, `town_to_element()`, `element_to_town()` et `xml_to_town()` ont été déplacées dans [town.py](town.py) : `element_to_town(element)` et `xml_to_town(file)` s’appellent désormais `from_element(element)` et `from_xml(file)`, et `town_to_xml(town, file)` et `town_to_element(town)` sont des méthodes de la classe `Town` et s’appellent respectivement `to_xml()` et `to_element()`.

## 2023-09-15 (fc6604ee) — Instructions d’utilisation dans le README
### Modifications
- Les instructions d’utilisation du [readme](README.md) sont désormais plus détaillées.

## 2023-09-15 (e8b34a8e) — Infection, manque, déshydratation
### Ajouts
- Les types de mort valides sont désormais stockés dans [citizen.py](citizen.py)`.death_reasons` : 5 sont pour l’instant acceptées, « En ville », « Dehors », « Infection », « Manque » et « Déshydratation ».
- La classe `Citizen` contient quelques nouvelles variables : `is_in_town` (booléen), `death_reason` (string, doit être dans la liste des types de mort valides) et `death_day` (entier, avec les mêmes règles que dans Hordes : toute mort autre qu’en ville compte pour une journée de moins).
### Modifications
- La fonction `attack()` de [citizen.py](citizen.py) a été séparée en plusieurs fonctions (`pre_attack()`, `attack()` et `post_attack()`), `pre` et `post` étant dédiées à l’évolution des états. La fonction à appeler depuis l’extérieur n’est plus `attack()`, mais `attack_wrapper()`.
- La fonction `die()` du même fichier prend désormais deux nouveaux arguments, `reason` et `day`.
- « Campeur avisé » est maintenant un statut valide dans [citizen.py](citizen.py)`.status_names`.
- Le simulateur de [simulateur.py](simulateur.py) `interactive_simulate_attacks()` calcule désormais le nombre de morts de chaque citoyen·ne par type de mort (par exemple, pour un citoyen donné, on saura qu’il est mort 50% du temps d’infection et 20% supplémentaires en ville). Ces informations ne sont affichées que si on est en mode `--verbose-stats`.
- Le [readme](README.md) a été mis à jour.

## 2023-09-14 (4e7cc11e) — Validation des états
### Ajouts
- La liste des états autorisés a été ajoutée à [citizen.py](citizen.py), et s’appelle `status_names`.
### Modifications
- La fonction `input_list()` de [simulateur.py](simulateur.py) accepte désormais des arguments `allowed_values`, `allowed_characters` et `custom_info`, qui fonctionnent de la même manière que ceux de `input_text()`.
- Les états des citoyens (ajoutés dans [simulateur.py](simulateur.py), dans les fonctions `interactive_edit_citizen()` et `interactive_create_citizen()`) doivent désormais être dans la liste des états autorisés.

## 2023-09-14 (3a9f05ac) — Arguments pour préciser l’attaque et les défenses
### Ajouts
- De nouveaux arguments peuvent être utilisés en appelant [simulateur.py](simulateur.py) :
    - `--defenses <int>` ou `-d <int>` permet de remplacer la valeur des défenses de base du preset par l’entier spécifié.
	- `--defenses <int>:<int>:<int>` ou `-d <int>:<int>:<int>` permet de régler également les défenses temporaires (le second nombre) ou dues à la veille (le troisième nombre). Il est possible de laisser vides certaines valeurs qu’on ne souhaite pas modifier (par exemple `-d ::50` pour définir les défenses de veille à 50).
    - `--attack <int>` ou `-a <int>` permet de définir la force de l’attaque (minimum et maximum) à une valeur spécifique.
	- `--attack <int>:<int>` ou `-a <int>:<int>` permet de régler séparément le minimum et le maximum. Tout comme pour `--defenses`, il est possible de ne pas spécifier une des deux valeurs pour conserver le réglage du preset.
### Modifications
- Dans [simulateur.py](simulateur.py), `force_continue` est désormais un `int` plutôt qu’un `bool`, pour permettre de passer plusieurs « mots » à la fois (par exemple pour `--copy-preset`, qui prend deux arguments).
- Mise à jour de la todo list du [README](README.md).
### Réparations
- L’argument `--samples` requiert désormais précisément la commande `--load-preset` plutôt que n’importe quelle commande.

## 2023-09-14 (c215ec00) — Édition interactive des presets
### Ajouts
- Dans [simulateur.py](simulateur.py), l’édition et la copie de presets de villes est désormais possible en mode interactif. C’est la fonction `interactive_edit_and_save_town()`, similaire dans sa structure à `interactive_create_and_save_town()`, qui permet d’exécuter les deux fonctions ; elle appelle en interne les fonctions `interactive_edit_town()` et `interactive_edit_citizen()`.
- Plusieurs fonctions utilitaires ont été ajoutées à [simulateur.py](simulateur.py) : elles permettent de demander un input à l’utilisateur avec diverses conditions (type, valeur par défaut, minimum / maximum…) et diminuent fortement la longueur du reste du code :
    - `input_int(prompt, default_value = None, original_value = None, min = None, max = None)` : Permet de demander un entier à l’utilisateur, et lui redemande tant qu’il ne donne pas un entier. `prompt` correspond à l’argument du même nom de la fonction `input()` ; `default_value` et `original_value` ont le même effet (remplacer un input vide de l’utilisateur par cette valeur) et ne doivent pas être utilisés en même temps (la différence entre les deux réside simplement dans le texte affiché à l’utilisateur) ; `min` et `max` déterminent le minimum et le maximum des valeurs acceptées, et sont tous les deux inclusifs.
	- `input_list(prompt, list, allow_duplicates = False)` : Permet de remplir une liste, entrée par entrée. `prompt` correspond à l’argument du même nom de la fonction `input()` et est affiché à chaque nouvelle entrée ; `list` est la liste à laquelle on souhaite ajouter des entrées ; et `allow_duplicates` peut être défini à `True` si on souhaite pouvoir ajouter plusieurs fois la même entrée à la liste.
	- `input_text(prompt, allowed_values = None, default_value = None, original_value = None, allowed_characters = None, custom_info = None)` : Permet de demander une entrée textuelle à l’utilisateur, et lui redemande tant qu’elle ne respecte pas les critères spécifiés. `prompt` fonctionne comme dans les cas précédents ; `allowed_values` peut être une liste de chaînes de caractères correspondant aux valeurs autorisées, ou `None` (qui signifie que toute chaîne de caractères est autorisée) ; `allowed_characters` peut être une chaîne de caractères contenant tous les caractères autorisés, ou valoir `None` (qui signifie que tous les caractères sont autorisés) ; `default_value` et `original_value` fonctionnent comme dans `input_int()`. Quant à `custom_info`, il s’agit d’une information entre parenthèses qui indique à l’utilisateur quel texte est attendu ; si `custom_info` vaut `None`, le programme affichera à la place la liste de toutes les chaînes autoriséees par `allowed_values` (ou rien du tout, si `allowed_values` vaut `None`).
- Dans [town.py](town.py), une nouvelle fonction, `remove_citizen()`, permet de supprimer un citoyen. Cette fonction attend un index dans la liste des citoyens, et non pas un nom de citoyen (car rien n’empêche le nom d’être en doublon).
### Modifications
- L’ajout des fonctions `input_int()`, `input_list()` et `input_text()` a permis de simplifier énormément les fonctions `interactive_create_town()`, `interactive_create_citizen()` et `interactive_simulate_attacks()`.
- Dans [citizen.py](citizen.py), la fonction `pack_statuses()` n’est désormais plus membre de la classe `Citizen`, afin de pouvoir être utilisée même sans référence à un citoyen.
### Réparations
- Dans [simulateur.py](simulateur.py), une condition impossible à valider terminait prématurément l’exécution de la commande `--copy-preset` (on comparait `preset_to_load` et `preset_to_save` avant de leur avoir assigné leurs valeurs).

## 2023-09-10 (11636ee1) — Mise à jour du README
### Modifications
- Le [readme](README.md) est désormais à jour au niveau des instructions d’utilisation.
- La partie `Todo` du readme a été convertie en liste de tâches.

## 2023-09-10 (6effa434) — Mise à jour de la page d’aide
### Modifications
- La page d’aide (accessible avec [simulateur.py](simulateur.py)`.print_usage()`) a été mise à jour et sa présentation a été améliorée.
- Deux nouvelles fonctions ont été ajoutées dans le même fichier, `print_line(n)` qui affiche une ligne de tirets cadratins de `n` caractères (par défaut 44), et `print_title()` qui affiche l’en-tête du programme.

## 2023-09-10 (4bff553c) — Réparation des états des citoyen·ne·s
### Réparations
- Dans [citizen.py](citizen.py), les états des citoyen·ne·s n’étaient pas correctement importés par la fonction `Citizen.__init__()`, et la liste de leurs états était toujours vide. Ce n’est désormais plus le cas.

## 2023-09-10 (13412eb1) — Indentation du XML exporté
### Réparations
- Le XML exporté par `town_to_xml()` et `citizen_to_xml()` (dans [simulateur.py](simulateur.py)) est désormais correctement indenté.

## 2023-09-10 (14a6050c) — Verbosité, distribution des probabilités
### Ajouts
- Dans [simulateur.py](simulateur.py), quelques détails supplémentaires ont été ajoutés et quelques autres sont maintenant optionnels (affichés uniquement si on est en mode « verbose ») :
    - `--verbose-details` ou `-vd` affiche les informations détaillées de la ville avant de lancer la simulation. Actuellement, cela détermine si on affiche ou non la liste des citoyens vivants.
	- `--verbose-stats` ou `-vs` affiche des informations statistiques plus complètes : au lieu de donner simplement moyenne, médiane, min et max, on affiche la distribution complète des probabilités de chaque nombre de morts.
### Modifications
- On évite de stocker une liste pour le calcul de la médiane dans [simulateur.py](simulateur.py).

## 2023-09-10 (852a2d04) — Création et chargement de presets
### Ajouts
- Il y a désormais des arguments de ligne de commande lorsqu’on appelle [simulateur.py](simulateur.py). Tous les arguments précédés d’un **X** sont mutuellement exclusifs :
    - **X** `--new-preset <name>`, `-np <name>` : crée un nouveau preset de ville en mode interactif, et l’enregistre sous le nom `<name>`
	- **X** `--edit-preset <name>`, `-ep <name>` : modifie un preset existant (en ajoutant / retirant des citoyens, en modifiant les défenses, la force de l’attaque, etc…). **Pas implémenté pour l’instant.**
	- **X** `--copy-preset <name1> <name2>`, `-cp <name1> <name2>` : comme edit-preset, mais modifie une copie de `<name1>` et enregistre cette version une fois modifiée sous le nom `<name2>`. **Pas implémenté pour l’instant.**
	- **X** `--load-preset <name>`, `-lp <name>` : lance une simulation à partir du preset `<name>`.
	- **X** `--help`, `-h` : affiche une page d’aide.
	- `--samples <samples>`, `-s <samples>` : règle le nombre d’essais à faire lors de la simulation (plutôt que de la faire en mode interactif). Ne fonctionne qu’avec les commandes qui lancent une simulation (pour l’instant seulement `--load-preset`).
- Dans [simulateur.py](simulateur.py), la fonction `town_to_xml(town, filename)` permet de convertir une ville en fichier XML, enregistré dans `presets/`. La fonction `xml_to_town(filename)` permet quant à elle d’ouvrir un de ces fichiers XML et de le convertir en instance de l’entité Town. Plusieurs sous-fonctions permettent de s’y retrouver un peu, mais n’ont pas vocation à être directement utilisées (`element_to_town(element)`, `element_to_citizen(element)`, `citizen_to_xml(citizen, file)`, `town_to_element(town)`…).
- L’affichage des résultats (dans la fonction `interactive_simulate_attacks()` de [simulateur.py](simulateur.py)) donne désormais plusieurs informations statistiques : la moyenne des morts, le minimum et le maximum possibles parmi tous les résultats obtenus, et la médiane desdits résultats.
- Le fichier [citizen.py](citizen.py) contient désormais une fonction `unpack_statuses(statuses)` qui convertit une chaîne de caractères (par exemple `"Terreur|Ivre|Drogué"`) en liste d’états (`["Terreur", "Ivre", "Drogué"]`).
- La classe `Citizen` du même fichier contient la fonction inverse, `pack_statuses()`, qui transforme la liste d’états stockée dans l’instance en chaîne de caractères.
- La fonction `print_info()` de [town.py](town.py) affiche désormais également la liste des citoyens, avec leurs informations (défense, décoration…).
- La classe `Citizen` est désormais capable d’afficher ses informations (défense, décoration…) de manière résumée, par le biais de la fonction `print_info()`.
- Une fonction de [town.py](town.py), `add_citizens(citizens_to_add)`, permet d’ajouter plusieurs citoyens à la fois à la liste.
- Ajout au [.gitignore](.gitignore) du dossier `presets/`, qui contient les presets enregistrés.
### Réparations
- Remplacement de toutes les espaces insécables par des espaces normales car je suis un glandu qui a trop vite pris l’habitude d’en mettre avant tous les deux-points.

## 2023-09-09 (e5bb0e92) — Présentation des résultats
- Toustes les citoyen·ne·s sont désormais listé·e·s à la fin d’une simulation, même celleux qui ne sont jamais mort·e·s.
- Les citoyen·ne·s sont désormais listé·e·s par chances de survie décroissantes.

## 2023-09-09 (af9a14d4) — Petites corrections
- Dans [simulateur.py](simulateur.py), `interactive()` a désormais bien une valeur de retour.
- Dans [town.py](town.py), `attack_hfr()` renvoie désormais une liste vide plutôt que 0 (on renvoyait encore le *nombre* de morts plutôt que la liste des citoyens morts). Cette fonction n’est de toute façon pas supposée être utilisée pour l’instant.

## 2023-09-09 (dae73e60) — Changelog et readme
- Création de ce **changelog**.
- Création d’une ébauche de [readme](README.md).

## 2023-09-09 (7289f847) — Version d’origine
- La classe `Citizen` (dans [citizen.py](citizen.py)) enregistre le pseudo, les défenses, la décoration et les statuts d’un·e citoyen·ne.
- `Citizen.attack(zombies, apply_results)` permet de vérifier si un citoyen survit ou non à une attaque de `zombies` zombies (elle renvoie `True` si tel est le cas). Cette fonction ne modifiera le citoyen que si `apply_results` vaut `True`.
- La classe `Town` (dans [town.py](town.py)) enregistre de nombreuses données sur la ville (nombre de citoyens, jour, attaque prévue, défenses…) ainsi que la liste des citoyens en vie.
- `Town.attack(apply_results, legacy)` permet d’exécuter une attaque sur la ville et renvoie une liste contenant les citoyen·ne·s qui sont mort·e·s pendant celle-ci. Si `apply_results` vaut `False`, la ville ne sera pas modifiée. Si `legacy` vaut `True`, on utilise à la place de la logique de MyHordes celle de Hordes S14 (non implémentée pour l’instant).
- Le fichier [simulateur.py](simulateur.py) contient la logique d’interaction avec l’utilisateur, et permet de créer une ville (en spécifiant les données nécessaires), de créer des citoyen·ne·s dans cette ville, et de lancer des simulations pour savoir quelles sont les chances de survivre de chaque citoyen·ne.